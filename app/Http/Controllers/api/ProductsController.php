<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    
    public function index(Request $request)
    {
        $products = Produto::all();

        return response()->json($products);

    }

    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
    
            'name' => 'required|max:255|min:2',
            'value' => 'required',
            'slog' => 'required',
            'description' => 'required|max:255|min:10',

            
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'não consegue ne, esta faltando algo'
            ], 400);
        }

        $product = Produto::create($request->all());

        return response()->json([
            'message' => 'Produto salvo meu rei',
            'product' => $product
        ], 201);

    }

    public function show(Produto $product)
    {
        return response()->json([
            'message' => 'deu bom',
            'Product' => $product
        ], 200);

    }


    public function update(Request $request, Produto $product)
    {
        $validated = Validator::make($request->all(), [
    
            'name' => 'required|max:255|min:2',
            'value' => 'required',
            'slog' => 'required',
            'description' => 'required|max:255|min:10',

            
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'nao consegue ne, esta faltando algo'
            ], 400);
        }

        $product->update($request->all());

        return response()->json([

            'message' => 'dados atualizado com sucessoo aee',
            'product' => $product


        ], 200);
    }

    public function destroy(Produto $product)
    {
        $product->delete();
        
        return response()->json([
            'message' => 'sumiuuu cadee a tete excluido com sucesso',
        ]);
    
    }
}
