# API-Products

APi desenvolvida em LARAVEL-9, com crud completo de produtos, tal api desenvolvida com intuito de avaliação academica.

## Como rodar o projeto:

### Requisitos:

1. PHP 8

1. LARAVEL 9

1. MYSQL

### Como ter o repositório:

2. link para o clone `https://gitlab.com/BiellBueno05/myfirstapi.git`

2. `composer install`

2. Configurar arquivo `.env`

2. Rodar o comando `php artisan generate:key`

2. Rodar o `PHP artisan migrate`

## Funcionalidades

### ROTAS:

![image.png](./image.png)

## Agora vamos fazer algumas requisições:

Vamos fazer umas requisições, para isso vamos utilizar o a paltaforma do `Postman`, mas se for de sua preferência você pode usar outra plataforma como o `insomnia`.

### STORE:

Para fazer o cadastro utiliza-se a requisição `POST`, na rota `http://127.0.0.1:8000/api/products/`, faremos um json para com os dados a serem salvos, para isso em **BODY** selecionamos **RAW** e usaremos a syntax de um arquido **JSON** e neles passamos as chaves esperado e seus respequitivos valores:

![image-1.png](./image-1.png)

Quando enviarmos apertando o botão **SEND**, esperamos a seguinte mensagem de sucesso, ja que os dados estão correto:

![image-2.png](./image-2.png)

As informações estaram no **BODY** de preferência no **PRETTY** por retorno o **JSON**.

### SHOW:

Vamos agora ver esse produto que cadastramos, para isso, faremos uma requisição `GET`, com mesma rota porem ao final dela informaremos o `ID` do produto que desejaremos ver:

![image-3.png](./image-3.png)

E teremos a mensagem de sucesso logo em seguida:

![image-5.png](./image-5.png)

### UPDATE

Vamos atualizar esses dados, usando uma requisição `PUT`, com a mesma rota do **SHOW** passando o `ID` e os todos os dados novamente e trocando aquele que deseja ser editado:


![image-6.png](./image-6.png)


Nesse caso trocamos o nome e o valor:
![image-7.png](./image-7.png)

A resposta de sucesso aguardada será:

![image-8.png](./image-8.png)

### INDEX

no rota `INDEX`vamos conseguir ver todos os produtos cadastrados, para isso é so usar a requisição `GET`novemente porem dessas vez não passaremos nenhum `ID`:

![image-9.png](./image-9.png)

As informações retornadas serão essas:

``` 
    [
        {
            "id": 1,
            "name": "mouse gamer",
            "value": 40.9,
            "slog": "012563632",
            "description": "um mouse ai po",
            "created_at": "2022-09-27T18:13:23.000000Z",
            "updated_at": "2022-09-27T19:06:04.000000Z"
        },
        {
            "id": 3,
            "name": "monitor 50pol",
            "value": 10000.5,
            "slog": "124565632",
            "description": "parece uma TV mas e um monitor",
            "created_at": "2022-09-27T18:39:00.000000Z",
            "updated_at": "2022-09-27T18:39:00.000000Z"
        },
        {
            "id": 5,
            "name": "teclado Gamer",
            "value": 50.5,
            "slog": "012565632",
            "description": "um teclado ai po",
            "created_at": "2022-10-04T12:45:15.000000Z",
            "updated_at": "2022-10-04T13:02:05.000000Z"
        }
    ]
``` 

### DELETE

Ultima das nossas Funcionalidades o `DELETE`, para ela usaremos a rota com `ID` novamente acompanhada da requisição `DELETE`:

![image-10.png](./image-10.png)

retornando a seguinte mensagem:

![image-11.png](./image-11.png)

#Mensagens de erro:

Caso falte algum dado em agluma requisoção, aguardatemos a seguinte mensagem:

![image-12.png](./image-12.png)

Para que essa mensagem seja apresentada, algum parametro não foi declarado, o minimo de caracteres que é de 2 letras não foi atendido ou o maximo excedido, no caso de 255 letras.

______________________________________________________________________________________________________________

Esta API está aberta a sugestões e a atualizações caso desejar entrar em contato estas são algumas redes sociais:

[![INSTA](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/bielbueno05/)
[![LINKDIN](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/gabriel-bueno-525b09209/)

